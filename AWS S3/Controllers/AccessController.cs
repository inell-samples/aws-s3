﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;

using AWSS3.Models;

namespace AWSS3.Controllers
{
    /// <summary>
    /// Provides authorization functionality
    /// </summary>
    public class AccessController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [HttpGet]
        public IActionResult Login(string ReturnUrl)
        {
            // If an User has been already authorized - go out of here!
            if (Request.HttpContext.User.Identity.IsAuthenticated)
                return RedirectToRoute("default", new { controller = "home" });

            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(AccessRequisitesModel AccessRequisites, string ReturnUrl)
        {
            await this.authenticate(AccessRequisites);

            if (Url.IsLocalUrl(ReturnUrl))
                return LocalRedirect(ReturnUrl);

            return RedirectToRoute("default", new { controller = "home" });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToRoute("default", new { controller = "home" });
        }

        /// <summary>
        /// Authenticate user with <see cref="AccessRequisitesModel"/>
        /// </summary>
        /// <param name="AccessRequisites"></param>
        /// <returns></returns>
        async Task authenticate(AccessRequisitesModel AccessRequisites)
        {
            // Create climes list
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, AccessRequisites.AccessKey),
                new Claim(ClaimTypes.Authentication, AccessRequisites.SecretKey)
            };

            // Create ClaimsIdentity object
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // Set authentication cookies
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id), new AuthenticationProperties()
            {
                IsPersistent = false,
            });
        }
    }
}
