﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSS3.Models
{
    public class AccessRequisitesModel
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }
}
